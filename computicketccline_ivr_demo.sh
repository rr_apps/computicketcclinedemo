#!/bin/sh

set -e

set >/tmp/cset
SNDDIR=/var/lib/asterisk/sounds/en/zapop/compuvent
OUT=/var/lib/asterisk/sounds/en/zapop/compuvent.sln16
SOXPADFMT='"|sox -n -r 44100 -c 1 -p trim 0.0 %s"'
SOXWAVFMT="\"${SNDDIR}/%s.wav\""

cd ${SNDDIR}
for f in *.wav
do
	f=${f%*.wav}
	fpfx=$( echo ${f} | cut -c 1-4 )
	eval "count=\${LIST_${fpfx}_count}"
	if [ -z "${count}" ]
	then
		count=1
		LISTS="${LISTS:+${LISTS} }${fpfx}"
	else
		count=$((${count}+1))
	fi
	eval "LIST_${fpfx}_${count}=${f}"
	eval "LIST_${fpfx}_count=${count}"
done

tc=2
for fpfx in ${LISTS}
do
	eval "count=\${LIST_${fpfx}_count}"
	if [ ${count} -gt 1 ]
	then
		rand=$(hexdump -n 1 -e '/1 "%u\n"' </dev/urandom)
		if [ "${fpfx}" = "ct09" ]
		then
			chosen=$(( (${rand}%(${count}-${tc})) + 1))
		else
			chosen=$(( (${rand}%${count}) + 1))
			if [ "${fpfx}" = "ct06" ]
			then
				tc=${chosen}
			fi
		fi
	else
		chosen=1
	fi
	eval "CLIP_${fpfx}=\${LIST_${fpfx}_${chosen}}"
	if [ "${fpfx}" = "ct09" ]
	then
		chosen=$(( ${chosen} + ${tc} ))
		eval "CLIP_${fpfx}_2=\${LIST_${fpfx}_${chosen}}"
	fi
done

SOXCMD="sox $(printf "${SOXWAVFMT}" ${CLIP_ct03}) $(printf "${SOXPADFMT}" 0.3)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct02}) $(printf "${SOXPADFMT}" 0.3)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct06}) $(printf "${SOXPADFMT}" 0.05)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct07}) $(printf "${SOXPADFMT}" 0.2)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct08}) $(printf "${SOXPADFMT}" 0.2)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct09}) $(printf "${SOXPADFMT}" 0.05)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct10}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct09_2}) $(printf "${SOXPADFMT}" 0.2)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct11}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct12}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct14}) $(printf "${SOXPADFMT}" 0.08)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct15}) $(printf "${SOXPADFMT}" 0.3)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct16}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct17}) $(printf "${SOXPADFMT}" 0.03)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct18}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct19}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct20}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct21}) $(printf "${SOXPADFMT}" 0.3)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct22}) $(printf "${SOXPADFMT}" 0.15)"
SOXCMD="${SOXCMD} $(printf "${SOXWAVFMT}" ${CLIP_ct23}) $(printf "${SOXPADFMT}" 1)"
SOXCMD="${SOXCMD} -t raw -c 1 -b 16 -r 16000 ${OUT}"

echo ${SOXCMD} |sh
