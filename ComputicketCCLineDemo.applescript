## Computicket Credit Card Booking Line - Demo

## To Do
# Create a solution for easier pad settings
# Create Pad creator sub routine
# Link paths via path to me

## Globals - Allows variable to be passed into subroutines
global theWavPath
global the_files
global pathMe
global pathTemp
global sOX

## Paths
# Path To This Process
set pathMe to path to me

## Deployed path
--set pathMe to (fileStuff(pathMe, 1) & "Contents/Resources/Scripts/")
## Developement path
set pathMe to (fileStuff(pathMe, 4) & "/")

# Path to the Directory containing the wav file to be concatenated
set theWavPath to pathMe & "wavs/"

# Path to the "temp" directory where the pad files and concatenated files will be placed
set pathTemp to pathMe & "temp/"

# Path to concatenated file
set concatFile to pathTemp & "concat.wav"
set masteredFile to pathTemp & "mastered.wav"

# Paths to Tools
set pathBin to pathMe & "libs/"
set lAme to pathBin & "lame"
set mp3Info to pathBin & "mp3info"
set mp3Splt to pathBin & "mp3splt"
set PlaY to pathBin & "play"
set ReC to pathBin & "rec"
set sOX to pathBin & "sox"
set SoXi to pathBin & "soxi"

## Options
set theOptions to {"Standard Settings", "Include Mastering", "Include Telephone Emulation", "Include Mastering & Tel Emulation"}
--> Choose process from a list
choose from list theOptions with prompt "Please Select A Process: " default items {"Include Mastering & Tel Emulation"}
set theOption to result as text

if theOption is false then return

if theOption is "Standard Settings" then
	set masterSwitch to "off"
	set telEmu to "off"
else if theOption is "Include Mastering" then
	set masterSwitch to "on"
	set telEmu to "off"
else if theOption is "Include Telephone Emulation" then
	set masterSwitch to "off"
	set telEmu to "on"
else if theOption is "Include Mastering & Tel Emulation" then
	set masterSwitch to "on"
	set telEmu to "on"
end if

##################################
## Event Convirmation

(*
 (pd1)	Your booking details are as follows,	(pd2)	30 Seconds to Mars (pd3) Concert	(pd4)	2 (pd5) Tickets (pd6) Seating (pd7)	P1	(pd8)	to (pd9) in pd10 Golden Circle Standing (pd11) at the (pd12)	Grand West Casino & Entertainment World	 (pd13) Sunday (pd14) 23 (pd15) November	(pd16) 2014 (pd17) at (pd18) 8:00PM (pd19) The cost of your tickets will be (pd20)	 510.00
*)

#Your booking details are as follows
property pd1 : 0.3
#30 Seconds to Mars
property pd2 : 0.3
#2
property pd3 : 0.05
#Tickets
property pd4 : 0.2

#Seating
property pd5 : 0
#P1
property pd6 : 0.05
#to
property pd7 : 0.15
#P3
property pd8 : 0.2
#in
property pd9 : 0.15
#Golden Circle Standing
property pd10 : 0.15
#at the
property pd11 : 0.03
#Grand West Casino & Entertainment World
property pd12 : 0.3

#Sunday
property pd13 : 0.15
#23
property pd14 : 0.03
#November
property pd15 : 0.15
#2014
property pd16 : 0.15
#at
property pd17 : 0.15
#8:00PM
property pd18 : 0.3

#The cost of your tickets will be
property pd19 : 0.15
#510.00
property pd20 : 1

# Select the files
set the_files to (list folder theWavPath without invisibles)

## msg "Your booking details are"
set part01 to �
	getElement(6) & space & padCreate("pad01", pd1) & space & �
	getRndElement(2, 5) & space & padCreate("pad02", pd2)

# msg "No Tickets"
set part02 to �
	getRndElement(11, 13) & space & padCreate("pad03", pd3) & space & �
	getElement(14) & space & padCreate("pad04", pd4)

# msg Seating P1 To P3 in  Golden Cirlce
set ct09setRndNo to getRndNumber(16, 30) -- get a random no range
set part03 to �
	getElement(15) & space & padCreate("pad05", pd4) & space & �
	getElement(ct09setRndNo) & space & padCreate("pad06", pd6) & space & �
	getElement(48) & space & padCreate("pad07", pd7) & space & �
	getElement(ct09setRndNo + 2) & space & padCreate("pad08", pd8) & space & �
	getElement(49) & space & padCreate("pad09", pd9) & space & �
	getRndElement(50, 57) & space & padCreate("pad10", pd10)

# msg At Location
set part04 to �
	getElement(58) & space & padCreate("pad11", pd11) & space & �
	getRndElement(59, 62) & space & padCreate("pad12", pd12)

# msg Day and Date
set part05 to �
	getRndElement(63, 67) & space & padCreate("pad13", pd13) & space & �
	getRndElement(68, 75) & space & padCreate("pad14", pd14) & space & �
	getRndElement(76, 79) & space & padCreate("pad15", pd15) & space & �
	getElement(80) & space & padCreate("pad16", pd16) & space & �
	getElement(81) & space & padCreate("pad17", pd17) & space & �
	getRndElement(82, 86) & space & padCreate("pad18", pd18)

# msg Cost of Tickets
set part06 to �
	getElement(87) & space & padCreate("pad19", pd19) & space & �
	getRndElement(88, 115) & space & padCreate("pad20", pd20)

# String the parts together that will make up the entire message
set soxProcess to �
	(sOX & space & part01 & space & part02 & space & part03 & space & part04 & space & part05 & space & part06 �
		& " -c 1 " & concatFile & " pad 0 0")

# Run the actual process using SoX to concatenate the parts
do shell script soxProcess

## End of Event Confirmation
##################################

##################################
## Post Event Confirmation

## Get the elements

## Join the Elements

## Create the Command

## End of Event Details
##################################

##################################
## Event Details

## Get the elements

## Join the Elements

## Create the Command

## End of Event Details
##################################

##################################
## Additional Event Details

## Get the elements

## Join the Elements

## Create the Command

## End of Additional Event Details
##################################

## Mastering -- These settings can be tweaked once the system goes live for optimal results.
# The mastering chain is as follows;
# 	Gain reduction to avoid clipping --> Compression (an artifact is the attack is fast so this results in a fade-in) --> EQ with boost in the lows and highs --> Normalisation

if masterSwitch is "on" then
	set audioGain to "gain -10"
	set audioComp to "compand 0,0.1 -60,-60,-30,-15,-20,-12,-4,-8,-2,-7 -2"
	set audioEQ to "equalizer 40 .71q +24 equalizer 80 1.10q +0 equalizer 240 1.80q -3 equalizer 500 .71q +0 equalizer 1000 2.90q +0 equalizer 4100 .51q +2.5 equalizer 8500 .71q +2.0 equalizer 17000 .71q +6"
	set audioNorm to "norm"
else
	set audioGain to "gain 0"
	set audioComp to ""
	set audioEQ to ""
	set audioNorm to ""
	set audioSinc to ""
	set audioRate to ""
end if

## Telephone Emulation -- These are for test purposes to better evaluate the results.
# The emulation chain is as follows;
# High pass filters at 300Hz & low pass filters at 3400Hz

if telEmu is "on" then
	set audioSinc to "sinc 300-3400"
	set audioRate to "-r 8k"
else
	set audioSinc to ""
	set audioRate to ""
end if


# Set the audio processing chain
set audioProc to audioGain & space & audioComp & space & audioEQ & space & audioSinc & space & audioNorm

# Set the command that SoX will run
set audioCmd to sOX & space & concatFile & space & audioRate & space & masteredFile & space & audioProc

# Run the actual process using SoX to concatenate the parts
do shell script audioCmd

##################################
## Play the Results

 set thePlay to POSIX file masteredFile as Unicode text
 tell application "Play Sound" to stop
 delay 0.3
 tell application "Play Sound"
 	play thePlay repeat 0
 end tell

#do shell script PlaY & space & masteredFile

## End of Play the Results
##################################

###############################################
##### 						Various Subroutines
###############################################

## Random number generator
on getRndNumber(lowNum, HighNum)
	set rndNo to (random number from lowNum to HighNum)
	return rndNo
end getRndNumber

## Random element picker
on getRndElement(lowNum, HighNum)
	set y to (random number from lowNum to HighNum)
	set x to item y of the_files
	set z to quoted form of (theWavPath & "/" & x)
	return z
end getRndElement

## Get the wave element based on list position.
on getElement(y)
	set x to item y of the_files
	set z to quoted form of (theWavPath & "/" & x)
	return z
end getElement

--> Zero Padding Subroutine
on zero_pad(value, string_length)
	set string_zeroes to ""
	set digits_to_pad to string_length - (length of (value as string))
	if digits_to_pad > 0 then
		repeat digits_to_pad times
			set string_zeroes to string_zeroes & "0" as string
		end repeat
	end if
	set padded_value to string_zeroes & value as string
	return padded_value
end zero_pad

## Get the file stuff.
# Get the file name, ext, path or path alias.
on fileStuff(theFile, theOption)
	-- Option (1)thePosixFile (2)fileName (3)fileExt (4)filePath (5)filePathAlias
	set theResults to {}
	set thePosixFile to POSIX path of theFile
	set fileName to (do shell script "basename  " & quoted form of thePosixFile)
	set fileExt to (do shell script "fileExt=$(echo " & quoted form of thePosixFile & " | sed -e 's/.*\\.//') ; echo $fileExt")
	set filePath to (do shell script "dirname " & quoted form of POSIX path of thePosixFile)
	set filePathAlias to POSIX file thePosixFile as alias
	set theResults to {thePosixFile, fileName, fileExt, filePath, filePathAlias}
	set theResult to item theOption of theResults
	return theResult
end fileStuff

## Use SoX to create pad files
on padCreate(padName, padLength)
	set padToCreate to pathTemp & padName & ".wav"
	do shell script sOX & " -n -b 16 -c 1 -r 44100 " & padToCreate & " trim 0.0 " & padLength
	return padToCreate
end padCreate